<?php
		if($_REQUEST['exit']) 
		{
			setcookie('id', '', time() - 60*60*24*30, '/',$_SERVER['SERVER_NAME']); 
			setcookie('hash', '', time() - 60*60*24*30, '/',$_SERVER['SERVER_NAME']);
			header('Location: index.php'); exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php 
ini_set("max_execution_time", "2400");
include 'agrolibCore.php';
 ?>
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Система автоматизированного реферирования многоязычных электронных массивов научно-технических публикаций по аграрной тематике</title>
	<meta name="keywords" content="automatic summarization, автоматическое реферирование">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="./Summarizer/bootstrap.min.css">
	<link href="./Summarizer/theme.css" rel="stylesheet" type="text/css" media="all">
	<link rel="stylesheet" href="./Summarizer/font-awesome.min.css">
    <link href="./Summarizer/fonts.css" rel="stylesheet" type="text/css" media="all">
    <script src="./Summarizer/jquery.js"></script>
    <script src="./Summarizer/bootstrap.min.js"></script>
	<link href="./Summarizer/default.css" rel="stylesheet" type="text/css" media="all">
	<link href="./Summarizer/belal.css" rel="stylesheet" type="text/css" media="all">
    <link type="text/css" rel="stylesheet" href="css/styles.css">
    <script type="text/javascript" src="/script/js/loadtext.js"></script>

<script type="text/javascript">
    function scrollToActiveLink() {
        var activeLink = document.getElementById("featured-wrapper2");
        if (activeLink == null) return true;
        var aDiv = activeLink.parentNode.parentNode;
        activeLink.scrollIntoView();
    }
    window.onload = scrollToActiveLink;

    function SetState() {
        var obj_checkbox = document.getElementById("url");
        var obj_textarea = document.getElementById("textar");
        var url_help = document.getElementById("urlhelp");
        if(obj_checkbox.checked)
        { obj_textarea.disabled = true;
            url_help.style.display = 'block'
        }
        else
        { obj_textarea.disabled = false;
            url_help.style.display = 'none'
        }
    }

    function disableExport() {
        document.getElementById("sbmt2").disabled = true;
    }

</script>
</head>
<body onload="chk();">
<div id="loading">
    <img id="loading-image" src="img/load.gif" alt="Loading..." />
</div>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container" style="background-color: #F6F4F3;"  >
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
	  <a class="navbar-brand" href="#"></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Реферирование текстов <span class="sr-only">(Текущая)</span></a></li>
        <!--<li><a href="history.php">Истоия рефератов</a></li>-->
		<li><a href="search.php">Поиск</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
 
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_COOKIE['login']; ?><span class="caret"></span></a>
          <ul class="dropdown-menu">
           <li><?php if ($trueadmin==true ){echo "<a href=\"/admin.php\">Администратор</a>";} else { echo "<p class=\"navbar-text\">Пользователь</p>";} ?></li>
          <li><form action="" method="post"><input style='margin:2%;width:96%;' type="submit" name='exit' value='Выйти' class="btn btn-default navbar-nav"/></form></li>
		 
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div style="min-height: 50px;"></div>
<div id="logo"   style="background-color:white; max-width: 1250px; margin-left: auto; margin-right: auto; border-radius: 20px; border: 1px solid green; "> 
	
	<h1 style="color:#00843e;"><img src="/img/logobelal.png" height ="60px"> Система автоматизи<wbr>рованного реферирования</h1>
	
</div>	

<div id="page" class="container" style="padding-right:15px; padding-left:15px;">

	<div class="title2">
		<h2>Реферирование текстов по аграрной тематике</h2>
			<div class="row">
			<div class="col-sm-6">
			<span class="byline">Введите текст или укажите файл для обработки в системе реферирования</span>
			</div>
			</br>
			
			</div>
	</div>
    <div class="row">
        <div class="col-sm-6 col-md-7">
            <form action="#result" role="form" enctype="multipart/form-data" method="POST" onkeyup="chk();" onmousemove="chk();" onclick="chk();">
			<script>
				function clear_textarea() {
				document.getElementById("textar").value = "";
			}
			
				function chk1(id) {
				return (document.getElementById(id).value == '');
			}
			
				function chk() {
				flag = chk1('textar') && chk1('url1') && chk1('InputFile');
				document.getElementById('sbmt').disabled = flag;
				return false;
			}
			
			function get_information(link, callback) {

				var xhr = new XMLHttpRequest();
				xhr.open("GET", link, true);
				xhr.onreadystatechange = function() {
						if (xhr.readyState === 4) {
							callback(xhr.responseText);
						}
						};
				xhr.send(null);
			}	

			function GetTextFromURL(){
                $(function () {
                    //запуск емитации загрузки пользователя
                    $('#geturl').click(function () {
                        if(document.getElementById('url1').value!=''){
                             ltext();
                        }
                    });
                });
				var url="";
				url="./GetTextHtml.php?url="+document.getElementById('url1').value;


				get_information(url, function(text) {
												
								document.getElementById('textar').value=text;						
								})
                //удаляем div загрузки текста
                alert('11');
                delload();
				return false;

			}
			
			function readFile(object) {
				var file = object.files[0];
				var reader = new FileReader();
				reader.onload = function() {
					document.getElementById('textar').value = reader.result;
				}

			reader.readAsText(file);
			}
			
			
			
						
			</script>
            <div id="lt" class="form-group">
            	<textarea class="form-control" style="resize:none" id="textar" name="mytext" cols="90" rows="23" placeholder="Введите текст и нажмите опцию Реферировать">
                    <?php #хранение текста после обновления страницы
					if( isset( $_POST['mytext'] ) )
					{echo $_POST['mytext'];}
				?></textarea>

                <input type="hidden" name="action" value="summarize">
                <div id="deltext" class="form-group"><input type="button" class="btn btn-default" style="background-color:#EEEEEE;" value="Удалить введенный текст" onclick="javascript:clear_textarea();"></div>
            </div>

           

        </div>
        <div class="col-sm-6 col-md-5">
		
			<div class="well well-sm" style="border-radius:0px">
				<div class="form-group">
					<span class="byline">Реферировать по URL:</span>
					<div class="form-inline">
					    <input type="text" id="url1" class="form-control" name="url_fieled" placeholder="Укажите URL" autocomplete="off" style="width:85%; display:inline-block;">
						<input type="button" name="GetUrl" id="geturl" class="btn btn-default" style="background-color:#EEEEEE; margin-left:5px; " value="&#8681;"  title="Получить текст сайта" onclick="javascript:GetTextFromURL();">
					</div>
				</div>
				<div class="form-group">
					<span class="byline">Выберите файл с жесткого диска:</span>
					<input type="file" id="InputFile" name="upfile" accept=".txt" onchange="readFile(document.getElementById('InputFile'));">
					<input type="hidden" name="action" value="upload">
				</div>
                <span class="byline">Укажите размер создаваемого реферата:</span>
                <div class="row">
                    <div class="col-sm-4">
                    <div class="input-group">
                        <input type="number" min="5" max="80" step="5" class="form-control" aria-describedby="sizing-addon2" name="sumsize" <?php if($_POST["sumsize"]>=5 and $_POST["sumsize"]<=80 ){echo 'value="'.$_POST["sumsize"].'" ';}else {echo 'value="20"';}?> autocomplete="off">
                        <span class="input-group-addon" id="sizing-addon2">%</span>
                    </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
                <span class="help-block">Данная опция задает процент предложений исходного текста, включаемый в реферат</span>
                <div class="checkbox">
                    <label><?php if (!isset($_POST['checkkeywords'])) $_POST['checkkeywords']='on' ?><input type="hidden" name="checkkeywords" value="off"><input <?php  if ($_POST['checkkeywords'] == 'on'  ) echo 'checked'; ?>  type="checkbox" name="checkkeywords" value="on">Показывать ключевые слова</label>
                </div>
                <div class="checkbox">
                    <label><input <?php if ($_POST['checkentity'] == 'on') echo 'checked'; ?>  type="checkbox" name="checkentity" value="on">Показывать выделенные именованные сущности</label>




                </div><!--
                <div class="checkbox">
                    <label title="Данная функция не доступна" style="color:#aaaaaa" disabled><input type="checkbox" name="kwis" value="on" disabled>Необходимо предварительное распознавание текста документа</label>
                </div>-->
                <div class="selectContainer">
                    <label>Язык документа</label>
                    <select class="form-control" name="lang">
                        <option  value="auto">Автоматическое определение</option>
                        <option <?php if($_POST["lang"]=='rus'){echo 'selected ';}?> value="rus">Русский</option>
                        <option <?php if($_POST["lang"]=='eng'){echo 'selected ';}?> value="eng">Английский</option>
                        <option <?php if($_POST["lang"]=='blr'){echo 'selected ';}?> value="blr">Белорусский</option>
                    </select>
                </div>
                <div class="selectContainer">
                <label>Тематика реферата</label>
				<?php
	#Загрузка из базы тем
	echo '	<select class="form-control" name="theme"> ';
		echo '	<option value="Общая">Общая</option> ';
$link = mysql_connect('localhost', 'root', '');#******************* настройки к БАЗЕ ДАННЫХ(Пока что только для заполения тем)

if (!$link) {
	echo ' </select>';
    #echo('Ошибка Соединения: ' . mysql_error());
	echo '<div class="alert alert-danger fade in" align=center style="position:absolute;top:0px; width:100%" >
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <strong>Ошибка базы данных</strong>
	  </div>';
}
else
{
mysql_select_db('agrolib_rus', $link) or die('Could not select database.');
mysql_query("SET NAMES 'utf8'");
$res = mysql_query('SELECT name FROM agrolib_rus.corpuses where ishidden=0 order by name', $link );

while ($line = mysql_fetch_array($res, MYSQL_ASSOC)) {
		$cell=$line['name'];
		echo "<option ";
		if (!empty($_POST["theme"])&&$_POST["theme"]==$cell){echo 'selected ';}
		echo "value=\"$cell\">$cell</option>";
}

 	
		

mysql_free_result($res);
mysql_close($link);
echo ' </select>';
}
?>
	            </div>
                <div id="bref" class=" col-sm-offset-4 col-md-offset-3">
                    <input  type="submit" name="SummuryBtn" id="sbmt" class="btn btn-default btn-sm btn-block"  value="Реферировать" disabled="">
                </div>
            </div>

        </div>

    </div>

	</form>
	<?php

 if( isset( $_POST['SummuryBtn'] ) )
    {
		$Sum =Summury($_POST["mytext"],$_POST["theme"],$_POST["lang"]);

		#$Sum =Summury($_POST["mytext"],"Общая");       
		#$Sum =Summury($_POST["mytext"],$_POST["theme"],$NumOfElements);     
		#echo('<h2>Реферат документа</h2>');	
		#echo('<p>'/*.$Sum->description.'<br><br>'*/.Pagereferat($Sum->sentences,7).'</p>');
		#echo('<p><b>Ключевые слова:</b> '.Keywords($Sum->sentences,10).'</p>');
		#echo('<p><b>Именованные сущности:</b> '.Entities($Sum->sentences).'</p>');
		#echo("</ul><br><hr>");
    }

if( isset( $_POST['SummuryBtn'] ) )
{
    /*echo "<script>$('body').append('<div style=\"\" id=\"loadingDiv\"><div class=\"loader\">Loading...</div></div>');
$(window).on('load', function(){
  setTimeout(removeLoader, 2000); //wait for page load PLUS two seconds.
});
function removeLoader(){
    $( \"#loadingDiv\" ).fadeOut(500, function() {
      // fadeOut complete. Remove the loading div
      $( \"#loadingDiv\" ).remove(); //makes page more lightweight 
  });  
}
     </script>";*/
echo "
</div>
    <div class=\"row\">
    <div class=\"col-sm-7\">
    </div>
    <div class=\"col-sm-5\"></div>
    </div>
</div>
<div id=\"featured-wrapper2\">
    <div id=\"featured\" class=\"container\">
        <div class=\"row\" id =\"result-ref\">
            <div class=\"col-sm-7\">
                <div class=\"panel panel-default\" style=\"border-radius:0px\">
                    <div class=\"panel-body\">
							<a name=\"result\"></a> 
                            <p><b>Язык документа:"; if($Sum->pageLang=='rus') echo 'Русский'; elseif($Sum->pageLang=='eng') echo 'Английский'; elseif($Sum->pageLang=='blr') echo 'Белорусский'; 
							echo "</b>";
							if($Sum->isAutoLang==1)echo'(определён автоматически)'; 
							else echo'(установлен вручную)';   
							echo "</p>
                            <p><b>Реферат документа</b></p>
                            <table><thead></thead><tbody><tr><td style=padding:5px align=\"justify\">" ;
							$page_referat=Pagereferat($Sum->sentences,7,$_POST['sumsize']);
							echo('<p>'.$page_referat.'</p>'); 
							echo ("</td></tr></tbody></table><br/>
                    </div>
                </div>
            </div>
			<div class=\"col-sm-5\">");
	if ($_POST['checkkeywords'] == 'on') {echo "           
                <div class=\"panel panel-default\" style=\"border-radius:0px\">
                    <div class=\"panel-body\">
                        <div>
                        <table class=\"table table-condensed\">
                        <thead><tr><th>Ключевые слова</th><th>Информативность</th></tr></thead>
                        <tbody> ";
							$key_arr=array();
							$key_arr_str='';
							$key_arr=Keywords_table($Sum->sentences,5);
							foreach ($key_arr as $arkey=>$ar)
							{
								$key_arr_str=$key_arr_str.$arkey.':'.$ar.';';
								echo('<tr><td>'.$arkey.'</td><td>'.$ar.'</td></tr>');
								
							}
						echo "
                        </tbody>
                        </table>
                        </div>
                    </div>
               </div>";}
			if ($_POST['checkentity'] == 'on') {echo " 
                <div class=\"panel panel-default\" style=\"border-radius:0px\">
                    <div class=\"panel-body\">
                        <div>
                        <table class=\"table table-condensed\">
                        <thead><tr><th>Именованные сущности</th><th>Тип</th></tr></thead>
                        <tbody>";
							$ent_arr=array();
							$ent_arr_str='';
							$ent_arr=Entities_table($Sum->sentences,10);
							foreach ($ent_arr as $arkey=>$ar)
							{
								$ent_arr_str=$ent_arr_str.$arkey.':'.$ar.';';	
								echo('<tr><td>'.$arkey.'</td><td>'.$ar.'</td></tr>');
							}
						echo "
                        </tbody>
                        </table>
                        </div>
                    </div>
                </div>";}
			echo "	
            </div>
        </div>
    </div>
</div>
";

$link = mysql_connect('localhost', 'root', '');
if ($link) {
mysql_select_db('agrolib_main', $link) or die('Could not select database.');
mysql_query("SET NAMES 'utf8'");


#$key_arr_str=implode(",",$key_arr);
#$ent_arr_str=implode(",",$ent_arr);
#echo $key_arr_str;
$res = mysql_query('INSERT INTO agrolib_main.result(datetime,text,summury,theme,language,keywords,entities,user,url,fileadress,date) VALUES(\''.date('c').'\',\''.$_POST["mytext"].'\',\''.$page_referat.'\',\''.$_POST["theme"].'\',\''.$Sum->pageLang.'\',\''.$key_arr_str.'\',\''.$ent_arr_str.'\',\''.$_COOKIE['login'].'\',\'\',\'\','.date('Ymd').')', $link );
mysql_close($link);
}
else{
echo '<div class="alert alert-danger fade in" align=center style="position:absolute;top:0px; width:100%" >
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <strong>Ошибка базы данных</strong>
	  </div>';
}

##start SAVE TO TXT 
$isSaveTxt=false;
if($isSaveTxt)
{

	$key_str=str_replace(";","\r\n",$key_arr_str);
	echo  $key_str;
	$myfile = fopen("thistext.txt", "w") or die("Unable to open file!");
	$txt = "Тема реферирования: ".$_POST["theme"]."\r\n".$_POST["mytext"]."\r\nСлова и информативности\r\n".$key_str;
	fwrite($myfile, $txt);
	fclose($myfile);
}
##end save to txt

}
#date('c') - текущая дата на сервере
#$_POST["mytext"] - текст
#$page_referat - реферат
#$_POST["theme"] - тема реферирования
#$Sum->pageLang - язык реферирования
#$key_arr - массив ключевых слов(нужно преобразовать в строку)
#$ent_arr - массив именованных сущностей
#без пользвателя (user)
#пока url=""
#пока fileadress=""

?>
</div>
<footer class="page-footer font-small special-color-dark pt-4" style="background-color: rgba(255,255,255); max-width:1175px; margin-left: auto; margin-right: auto;">
<div class="footer-copyright text-center py-3" style="background-color: rgba(0,0,0,0.1); padding:15px; max-width:1175px; margin-left: auto; margin-right: auto;  border: 1px solid gray;">
Разработано: <b>ОИПИ НАН Беларуси</b> 2018г
</footer>
<script language="javascript" type="text/javascript">
    $(window).load(function() {
        //запуск емитации загрузки контента
        $('#loading').hide();
    });
</script>
<!--script language="javascript" type="text/javascript">
        $(function () {
            //запуск емитации загрузки пользователя
            $('#geturl').click(function () {
             /*if(document.getElementById('url1').value!=''){
                  ltext();
             }*/
            });
        });

</script-->
</body></html>