<?php
		if($_REQUEST['exit']) 
		{
			setcookie('id', '', time() - 60*60*24*30, '/',$_SERVER['SERVER_NAME']); 
			setcookie('hash', '', time() - 60*60*24*30, '/',$_SERVER['SERVER_NAME']);
			header('Location: index.php'); exit();
		}
?>
<script>
	function GetRef(idref){
	
		var url="";
		url="./search_ref.php?id_ref="+idref;
		$.get(url, function(text) {									
						document.getElementById('textref').innerHTML=text;
						})
						
		return false;
	}
	function Mouse1(){
	
	document.getElementsByClassName("history").style.cursor = "pointer";
	
	}
	function Mouseauto(){
	style.cursor = "auto";
	
	}
	document.getElementsByClassName("history").style.cursor = "pointer";
				
</script>
<html>
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>История -- Система автоматизированного реферирования многоязычных электронных массивов научно-технических публикаций по аграрной тематике</title>    
	<meta name="keywords" content="automatic summarization, автоматическое реферирование">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="./Summarizer/bootstrap.min.css">
	<link href="./Summarizer/theme.css" rel="stylesheet" type="text/css" media="all">
	<link rel="stylesheet" href="./Summarizer/font-awesome.min.css">
    <link href="./Summarizer/fonts.css" rel="stylesheet" type="text/css" media="all">
    <script src="./Summarizer/jquery.js"></script>
    <script src="./Summarizer/bootstrap.min.js"></script>
	<link href="./Summarizer/default.css" rel="stylesheet" type="text/css" media="all">
	<link href="./Summarizer/belal.css" rel="stylesheet" type="text/css" media="all">
  </head>
  <body>
  <nav class="navbar navbar-default navbar-fixed-top" >
  <div class="container" >
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
	  <a class="navbar-brand" href="#"></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="/">Реферирование текстов </a></li>
        <!--<li><a href="history.php">Истоия рефератов</a></li>-->
		<li class="active"><a href="search.php">Поиск<span class="sr-only">(Текущая)</span></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
 
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_COOKIE['login']; ?><span class="caret"></span></a>
          <ul class="dropdown-menu">
           <li><?php if ($trueadmin==true ){echo "<a href=\"/admin.php\">Администратор</a>";} else { echo "<p class=\"navbar-text\">Пользователь</p>";} ?></li>
          <li><form action="" method="post"><input style='margin:2%;width:96%;' type="submit" name='exit' value='Выйти' class="btn btn-default navbar-nav"/></form></li>
		 
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div style="min-height: 50px; margin-bottom: 20px;"></div> 
	<div class='container' align="center"  style ="max-width:1200px; padding-top:15px;">
		<div class="title2">
			<h2>Поиск</h2>
		</div>
		</br>
		<div class="row">
			<form action="" method="post">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="checkbox">
                    <label>Ключевое слово</label>
					<div class="input-group">
						<span class="input-group-addon">
							<input type="checkbox" id="checkkeywords" name="checkkeywords" style="margin-left: -5px; margin-top: -5;" aria-label="..." <?php if( isset( $_POST['checkkeywords'] )){echo checked;}  ?> >
						</span>
						<input type="text"  id="textkeywords" value="<?php if( isset( $_POST['textkeywords'] )){echo $_POST['textkeywords'];} ?>" name="textkeywords" class="form-control" aria-label="..." onclick="check('checkkeywords')">
					</div><!-- /input-group -->
                </div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="checkbox">
                    <label>Именованные сущности</label>
					 <div class="input-group">
						<span class="input-group-addon">
							<input type="checkbox" id="checkentity" name="checkentity" style="margin-left: -5px; margin-top: -5;" aria-label="..." <?php if( isset( $_POST['checkentity'] )){echo checked;}  ?> >
						</span>
						<input type="text"  id="textentity" name="textentity" value="<?php if( isset( $_POST['textentity'] )){echo $_POST['textentity'];} ?>" class="form-control" aria-label="..." onclick="check('checkentity')">
					</div><!-- /input-group -->
                </div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12" >
			<div class="input-group">
				Дата
				</br>
				&nbsp; с
				<input id="datestart" name="datestart" type="date" value=<?php if( isset( $_POST['datestart'] )){echo $_POST['datestart'];}else{echo "2017-01-01";} ?> ></br>
				по
				<input id="dateend" name="dateend" type="date" value="<?php if( isset( $_POST['dateend'] )){echo $_POST['dateend'];}else{echo date('Y-m-d');} ?>" >
			</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
                <input type="submit" name="SearchButton" id="srch" class="btn btn-default" style="background-color:#EEEEEE; margin:20px;" value="Поиск в базе" >
			</div>
		</form>
		</div>
		</br>
		<div class="row">
			<div class="col-sm-4" align="left">			
				<div class="panel panel-default">
					<div class="panel-heading">История</div>
					<div class="panel-body" style="padding-left:0;">
    
		<?php
		$link = @mysql_connect(DB_HOST, DB_LOGIN, DB_PASSWORD);
		if (!$link) {
		#echo('Ошибка Соединения: ' . mysql_error());
		echo '<div class="alert alert-danger fade in" align=center style="position:fixed; left:25%; top:155;  width:50%;" >
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Ошибка базы данных</strong>
		</div>';
		}
		else
		{
		mysql_select_db('agrolib_main', $link) or die('Could not select database.');
		mysql_query("SET NAMES 'utf8'");
		if( isset( $_POST['SearchButton'] ) )
		{	     
			$querystr='SELECT id,date,summury,user,url,fileadress FROM agrolib_main.result WHERE user=\''.$_COOKIE['login'].'\' and date>='.str_replace("-","",$_POST['datestart']).' and date<='.str_replace("-","",$_POST['dateend']);
			if( isset( $_POST['checkentity'] ) )
			{			
				$querystr=$querystr.' and entities like \'%'.$_POST['textentity'].':%\'';
			}
			if( isset( $_POST['checkkeywords'] ) )
			{			
				$querystr=$querystr.' and keywords like \'%'.$_POST['textkeywords'].'%\'';
			}
			$querystr=$querystr.' ORDER BY id DESC';
			$res = mysql_query($querystr, $link );
			
		}
		else
		{
			$res = mysql_query('SELECT id,date,summury,user,url,fileadress FROM agrolib_main.result WHERE user=\''.$_COOKIE['login'].'\' and date>0  ORDER BY id DESC', $link );	
		}
		$curdate=0;
		$number=0;
		
		while ($line = mysql_fetch_array($res, MYSQL_ASSOC)) {
		$number+=1;
		$tbl_id=$line['id'];
		$tbl_date=$line['date'];
		#$tbl_datetime=$line['datetime'];
		#$tbl_text=$line['text'];
		$tbl_summury=$line['summury'];
		#$tbl_theme=$line['theme'];
		#$tbl_language=$line['language'];
		#$tbl_keywords=$line['keywords'];
		#$tbl_entities=$line['entities'];
		#$tbl_user=$line['user'];
		$tbl_url=$line['url'];
		$tbl_fileadress=$line['fileadress'];   

		$list_keywords=explode(";",$tbl_keywords);
		array_pop($list_keywords);
		$list_entities=explode(";",$tbl_entities);
		array_pop($list_entities);
		$tbl_keywords = str_replace(array(":",";"),array(" (","); "),$tbl_keywords);
		if ($number==1)
		{
		echo"<script>
				GetRef(".$tbl_id.");
		</script>";
		}
		if ($tbl_date==$curdate)
		{
		 echo "<li style = 'cursor: pointer;' onclick=\"GetRef(".$tbl_id.")\">".substr($tbl_summury,0,40)."...</li>
		 ";
		}
		else
		{
			if ($number>1)
			{echo"
			</li></ul>";}
			else 
			{
				echo"<ul>";
			}
			echo"
			<li>".$tbl_date."
			<ul>
				<li style = 'cursor: pointer;'  onclick=\"GetRef(".$tbl_id.")\" >".substr($tbl_summury,0,40)."...</li>
				";
			$curdate=$tbl_date;
		}		
		}
		echo "</ul>";
			
		
		}
		?>
			
			 </div>
			</div>
			</div>
			<div class="col-sm-8" >
			
			<div class="panel panel-default">
					<div class="panel-heading">Текст</div>
					<div class="panel-body">
					<h2>Реферат</h2>
					<span id="textref">
					
					<h3>Ключевые слова: </h3>
 
					</span>
					</div>
				
			</div>
			</div>
		</div>
  
		</br>
		</br>
	</div> 
  
 <?php
 echo "Дата-время обновления страницы: ".date('c');
 ?> 
</div>
<script language="javascript">
   
	function check(checkBoxID) {
    document.getElementById(checkBoxID).checked = true;
	
	}
	document.getElementsByClassName("history").style.cursor = "pointer";
</script>
</body>
</html>