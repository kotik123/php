<html>
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>История -- Система автоматизированного реферирования многоязычных электронных массивов научно-технических публикаций по аграрной тематике</title>    
	<meta name="keywords" content="automatic summarization, автоматическое реферирование">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="./Summarizer/bootstrap.min.css">
	<link href="./Summarizer/theme.css" rel="stylesheet" type="text/css" media="all">
	<link rel="stylesheet" href="./Summarizer/font-awesome.min.css">
    <link href="./Summarizer/fonts.css" rel="stylesheet" type="text/css" media="all">
    <script src="./Summarizer/jquery.js"></script>
    <script src="./Summarizer/bootstrap.min.js"></script>
	<link href="./Summarizer/default.css" rel="stylesheet" type="text/css" media="all">
  </head>
  <body>
  <nav class="navbar navbar-default navbar-fixed-top" >
  <div class="container" >
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
	  <a class="navbar-brand" href="#"></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="/">Реферирование текстов </a></li>
        <li class="active"><a href="history.php">Истоия рефератов<span class="sr-only">(Текущая)</span></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
 
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_COOKIE['login']; ?><span class="caret"></span></a>
          <ul class="dropdown-menu">
           <li><p class="navbar-text"><?php echo "Администрирование"; ?></p></li>
          <li><form action="" method="post"><input style='margin-left:10px;' type="submit" name='exit' value='Выйти' class="btn btn-default navbar-nav"/></form></li>
		 
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div style="min-height: 50px; margin-bottom: 20px;"></div> 
 <div class='container' align="center"  style ="max-width:1000px;">
  <h1><span>История рефератов</span></h1>
  </br>
  <table class="table table-striped js-options-table"><tr>
	<table class="table table-bordered table-striped js-options-table">
		<tr>
			<td rowspan="2" style="margin:0px; padding:5px;">
			<!-- Button trigger modal -->
			<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal1" style="margin:0px; padding:5px; height:100%;">+<br>+<br>+<br></button></td><td>Тут ставят дату</td><td>Язык текста</td><td>Тематика</td><td>Пользователь</td><td>Ключевые слова</td><td>Источник</td>
		</tr>
		<tr>
			<td colspan="6">Сокращённый вариант реферата текста остальное в модальном окне Сокращённый вариант реферата текста остальное в модальном окнеСокращённый вариант реферата текста остальное в модальном окнеСокращённый вариант реферата текста остальное в модальном окнеСокращённый вариант реферата текста остальное в модальном окнеСокращённый вариант реферата текста остальное в модальном окнеСокращённый вариант реферата текста остальное в модальном окнеСокращённый вариант реферата текста остальное в модальном окнеСокращённый вариант реферата текста остальное в модальном окне</td>
		</tr>
		<div class="modal fade bs-example-modal-lg" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-lg">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Полная информация</h4>
			</div>
			<div class="modal-body">
				<div class="row">
				<div class="panel panel-default" style="border-radius:0px">
				<div class="panel-body">
					<div class="col-sm-3">
					<p>(Дата)</p>
					</div>
					<div class="col-sm-3">
					<b>Язык текста:</b>
					<p>(Rus)</p>
					</div>
					<div class="col-sm-3">
					<b>Тематика реферата:</b>
					<p>(ОБщая)</p>
					</div>
					<div class="col-sm-3">
					<b>Пользователь:</b>
					<p>(user)</p>
					</div>
				</div>
				</div>
				</div>
				<div class="row">
					<div class="col-sm-8">
					<div class="panel panel-default" style="border-radius:0px">
					<div class="panel-body">						
						<p><b>Язык документа:Русский</b>(определён автоматически)</p>
						<h4><b>Реферат документа</b><h4>
						<p> Текст для реферирования чтобы увидеть стиль</p>
                    </div>
					</div>
					</div>
					<div class="col-sm-4">           
						<div class="panel panel-default" style="border-radius:0px">
						<div class="panel-body" align="left">
							<h4 align="center">Ключевые слова (Информативность)</h4>
							<ul>
							<li>увидеть (50)</li>
							<li>услышать (25)</li>
							</ul>
						</div>
						</div> 
						<div class="panel panel-default" style="border-radius:0px">
						<div class="panel-body" align="left">
							<h4 align="center">Именованные сущности (Тип)</h4>
							<ul>
							<li>Ломоносов (Person)</li>
							<li>Пушкин (Person)</li>
							</ul>
						</div>
						</div>	
					</div>
				</div>	
				<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-default" style="border-radius:0px">
					<div class="panel-body" align="justify">
						<h4><b>Полный текст документа</b></h4>
						<p>Сюда вписывается полный текст документа который был прореферирован</p>
					</div>
					</div>
				</div>
				</div>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
			</div>
			</div>
			</div>
		</div>	
	</table>
	</tr>
  <tr>
  </tr>
  <tr>
  </tr>
  </table>
  
  
  
<?PHP
echo "Дата-время обновления страницы: ".date('c');
$link = mysql_connect('localhost', 'root', '111');
if (!$link) {
#echo('Ошибка Соединения: ' . mysql_error());
echo '<div class="alert alert-danger fade in" align=center style="position:absolute;top:0px; width:100%" >
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <strong>Ошибка базы данных</strong>
	  </div>';
}
else
{
mysql_select_db('agrolib_main', $link) or die('Could not select database.');
mysql_query("SET NAMES 'utf8'");
$res = mysql_query('SELECT datetime,text,summury,theme,language,keywords,entities,user,url,fileadress FROM agrolib_main.result ORDER BY id DESC', $link );

echo("<table cellspacing=\"2\" border=\"1\" cellpadding=\"5\" width=\"600\" class=\"table\">
<thead><tr><th>datetime</th><th>text</th><th>summury</th><th>theme</th><th>language</th><th>keywords</th><th>entities</th><th>user</th><th>url</th><th>fileadress</th></tr></thead>
<tbody> ");
while ($line = mysql_fetch_array($res, MYSQL_ASSOC)) {
		$tbl_datetime=$line['datetime'];
		$tbl_text=$line['text'];
		$tbl_summury=$line['summury'];
		$tbl_theme=$line['theme'];
		$tbl_language=$line['language'];
		$tbl_keywords=$line['keywords'];
		$tbl_entities=$line['entities'];
		$tbl_user=$line['user'];
		$tbl_url=$line['url'];
		$tbl_fileadress=$line['fileadress'];
		     
	echo('<tr><td>'.$tbl_datetime.'</td><td>'.$tbl_text.'</td><td>'.$tbl_summury.'</td><td>'.$tbl_theme.'</td><td>'.$tbl_language.'</td><td>'.$tbl_keywords.'</td><td>'.$tbl_entities.'</td><td>'.$tbl_user.'</td><td>'.$tbl_url.'</td><td>'.$tbl_fileadress.'</td></tr>');

		
		
}
echo "
         </tbody>
    </table>";
	
}
?>
</div>
	</body>
</html>